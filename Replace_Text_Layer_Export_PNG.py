#!/usr/bin/env python
# Filename: Replace_Text_Layer_Export_PNG.py
# -*- coding: utf-8 -*-


"""

Found working PoC from https://protips.janeczko.io/graphics/utilities/2016/07/16/gimp-python-scripting.html
and simply modified it for my own needs

GIMP 2.8.18 Python Console
Python 2.7.13 (default, Sep 26 2018, 18:42:22)
[GCC 6.3.0 20170516]
>>> gimp.image_list()
[<gimp.Image '[Untitled]'>, <gimp.Image '[Untitled]'>, <gimp.Image '[Untitled]'>, <gimp.Image 'Red_hitsplat_xcf.xcf'>]
>>> original_image = gimp.image_list()[3]
>>> image = pdb.gimp_image_duplicate(original_image)
>>> text_layer = pdb.gimp_image_get_layer_by_name(image, 'LAYERNAMEHERE')
>>> pdb.gimp_text_layer_set_text(text_layer, 'NEWTEXTHERE"')
>>> pdb.gimp_text_layer_set_font_size(text_layer, 10, 0) # Font Size
>>> pdb.gimp_text_layer_set_font(text_layer, 'Sans') # Font Style
>>> merged_layer = pdb.gimp_image_merge_visible_layers(image, 1) # Flatten image before export; You could just export this layer if you wanted
>>> filename = 'test.png'
>>> pdb.file_png_save_defaults(image, merged_layer, '/path/to/file/' + filename, filename) # Export PNG
>>>

"""


# Insert 2 -> 130 text to image; Export as PNG

"""
>>> gimp.image_list()
[<gimp.Image 'Red_hitsplat_xcf.xcf'>]
"""

image_index_n = 0 # Start at image index

for i in range(2, 131):
    original_image = gimp.image_list()[image_index_n]
    image = pdb.gimp_image_duplicate(original_image)
    text_layer = pdb.gimp_image_get_layer_by_name(image, 'splat')
    pdb.gimp_text_layer_set_text(text_layer, str(i)) # Text string
    pdb.gimp_text_layer_set_font_size(text_layer, 10, 0) # Font Size
    pdb.gimp_text_layer_set_font(text_layer, 'Sans') # Font Type
    if len(str(i)) == 2: # Move text element left since double digit
        pdb.gimp_layer_set_offsets(text_layer, 5, 6)
    elif len(str(i)) == 3: # Move text element even further left since triple digit
        pdb.gimp_layer_set_offsets(text_layer, 4, 6)
        pdb.gimp_text_layer_set_font_size(text_layer, 8, 0) # Make smaller font to fit better
    merged_layer = pdb.gimp_image_merge_visible_layers(image, 1)
    filename = 'Red_hitsplat%d.png' % i
    pdb.file_png_save_defaults(image, merged_layer, '/home/meganerd/Documents/MEGANERD-GITLAB/yggdrasil/Dependencies/OSRS Hitsplat/' + filename, filename)
    image_index_n += 1





# Scale layer
#pdb.gimp_layer_scale(text_layer, 100, 100, True) # Width, Height

# Flatten Image
#Flat_Layer = pdb.gimp_image_flatten(Image)

# layer reorder
"""
for layer in reversed(image.layers):
    if layer.linked:
       pdb.gimp_image_reorder_item(img, layer, None, 0)
"""