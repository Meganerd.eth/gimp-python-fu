#!/usr/bin/env python

# Source: https://www.lf-empire.de/forum/showthread.php?tid=10521
 
import math, glob, struct
from gimpfu import *
 
gettext.install("gimp20-python", gimp.locale_directory, unicode=True)
 
V4_HEADER_SIZE = 108
COLOR_INFO_SIZE = 68
HEADER_OFF = 14
DATA_OFF_FIELD = 10
SIZE_OFF = 2
 
def strip_color_info(old_bmp_name, new_bmp_name=None):
	if new_bmp_name is None:
		new_bmp_name = old_bmp_name
	data = bytearray(open(old_bmp_name, "rb").read())
	header_size = struct.unpack("I", data[HEADER_OFF: HEADER_OFF + 4])[0]
	if header_size == 108:
		# Remove 68	 - the size for the extra data-chunk from both headers
		data[HEADER_OFF: HEADER_OFF + 4] = struct.pack("I", V4_HEADER_SIZE - COLOR_INFO_SIZE)
		data[DATA_OFF_FIELD: DATA_OFF_FIELD + 4] = struct.pack("I",
			struct.unpack("I",data[DATA_OFF_FIELD: DATA_OFF_FIELD + 4])[0] - COLOR_INFO_SIZE)
		# Offset image data:
		data[HEADER_OFF + header_size - COLOR_INFO_SIZE:] = data[HEADER_OFF + header_size:]
		data[SIZE_OFF: SIZE_OFF + 4] = struct.pack("I", len(data))
	with open(new_bmp_name, "wb") as output_file:
		output_file.write(data)
 
def batch_convert_rgb(pattern):
	for file in glob.glob(pattern):
		image = pdb.gimp_file_load(file, RUN_NONINTERACTIVE)
		drawable = pdb.gimp_image_get_active_layer(image)
		if pdb.gimp_image_base_type(image) != RGB:
			pdb.gimp_convert_rgb(image)
			pdb.gimp_file_save(image, drawable, file, file)
		pdb.gimp_image_delete(image)
		strip_color_info(file)
 
register(
		"batch_convert_rgb",
		"Batch process files and convert to RGB color palette",
		"Batch process files and convert to RGB color palette",
		"NightmareX1337",
		"NightmareX1337",
		"2016",
		"<Toolbox>/LF2 Tools/Batch Convert _RGB...",
		None,
		[
				(PF_STRING, "pattern", "File Glob", "*.bmp")
		],
		[],
		batch_convert_rgb)
 
main()